---
title: "VIGVAM"
---

Je nezisková volnočasová organizace založená v lednu 2016 jako zapsaný ústav 
v&nbsp;návaznosti na Centrum Břežánek. Jejím primárním posláním je podpora komunity napříč věkovým spektrem se zvláštním důrazem na teenagery, dospělé a seniory. Spolupodílí se na provozování komunitního centra, organizuje pravidelné kroužky, kurzy, přednášky a pořádá jednorázové kulturní, společenské či sportovní akce. Spolupracuje s&nbsp;místními spolky a organizacemi.

Součástí Senior klubu organizace VIGVAM, z.ú.je od ledna 2016 spolek Senin.
